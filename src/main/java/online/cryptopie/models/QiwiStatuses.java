package online.cryptopie.models;

public enum QiwiStatuses {
    DECLINED,
    REPEAT,
    MANUAL,
    OK,
    ACCEPTED
}
