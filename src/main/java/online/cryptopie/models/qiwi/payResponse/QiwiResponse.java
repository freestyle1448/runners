package online.cryptopie.models.qiwi.payResponse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.models.qiwi.userResponse.ResultCode;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class QiwiResponse {
    private ResponsePayment payment;
    @JacksonXmlElementWrapper(localName = "balances")
    private List<Balance> balances;
    @JsonProperty("result-code")
    private ResultCode result_code;
    @JacksonXmlElementWrapper(localName = "balances-ovd")
    private List<online.cryptopie.models.qiwi.ping.BalanceOvd> balancesOvd;
}
