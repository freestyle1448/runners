package online.cryptopie.models.qiwi.checkStatus;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.models.qiwi.payResponse.Balance;
import online.cryptopie.models.qiwi.userResponse.ResultCode;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonPropertyOrder({"result_code", "payment", "balances"})
public class Response {
    @JsonProperty("result-code")
    private ResultCode result_code;
    private PaymentResp payment;

    private List<Balance> balances;
}
