package online.cryptopie.models;

import lombok.SneakyThrows;
import online.cryptopie.models.qiwi.payResponse.QiwiResponse;
import online.cryptopie.models.qiwi.ping.Response;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.tasks.QiwiTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.Date;
import java.util.concurrent.CompletableFuture;

import static online.cryptopie.models.transaction.Status.*;
import static online.cryptopie.services.QiwiServiceImpl.getCurrency;

@Component
public class QiwiBean {
    private static final String BAD_MES = "Списание средств неуспешно для транзакции - {}, статус - {}, сообщение - {}";
    private static final String BANK_DECLINED = "Платеж отклонён банком";

    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);

    private final RetryTemplate retryTemplate;
    private final PlatformTransactionManager transactionManager;
    private final ManualRepository manualRepository;

    public QiwiBean(RetryTemplate retryTemplate, PlatformTransactionManager transactionManager, ManualRepository manualRepository) {
        this.retryTemplate = retryTemplate;
        this.transactionManager = transactionManager;
        this.manualRepository = manualRepository;
    }

    public void handlePing(Response ping) {
        ping.getBalances().forEach(balance -> {
            if (balance.getCode().equals(ping.getCurrency())) {
                manualRepository.findAndModifyGateSet(ping.getObjectId(), online.cryptopie.models.transaction.Balance
                        .builder()
                        .amount((long) (Double.parseDouble(balance.getValue()) * 100))
                        .build());
            }
        });
    }

    public CompletableFuture<Transaction> handleWithdraw(QiwiResponse response1, Transaction transaction) {

        if (response1.getPayment() != null) {
            final var isFatal = response1.getPayment().getFatal_error();
            final var isFinal = response1.getPayment().getFinal_status();
            final var status = response1.getPayment().getStatus();
            final var message = response1.getPayment().getMessage();

            switch (checkQiwiStatus(isFatal, isFinal, status)) {
                case DECLINED:
                    logger.info(BAD_MES
                            , transaction, status, message);
                    declineTransaction(transaction, BANK_DECLINED, message);
                    break;
                case REPEAT:
                    saveTransaction(transaction, 3, IN_PROCESS, "", "");
                    break;
                case MANUAL:
                    logger.info("Списание требует ручной обработки - {}, статус - {}"
                            , transaction, status);
                    saveTransaction(transaction, 0, MANUAL, "Ожидает подтверждения Банка", message);
                    break;
                case OK:
                    logger.info("Заявка на списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);

                    saveTransaction(transaction, 1, IN_PROCESS, "", "");
                    updateQiwiBalance(response1, transaction);
                    break;
                case ACCEPTED:
                    handleSuccessOperation(response1, transaction);
                    break;
            }

            return CompletableFuture.completedFuture(transaction);
        } else {
            if (response1.getResult_code() != null) {
                if (!response1.getResult_code().getValue().equals("0")) {
                    logger.info("Списание средств неуспешно для транзакции - {}, статус - {}"
                            , transaction, response1.getResult_code().getValue());

                    declineTransaction(transaction, BANK_DECLINED, response1.getResult_code().getValue());
                } else {
                    saveTransaction(transaction, 3, IN_PROCESS, "", "");
                }

                return CompletableFuture.completedFuture(transaction);
            }
        }

        return CompletableFuture.completedFuture(transaction);
    }

    public CompletableFuture<Transaction> handleCheckTransaction(QiwiResponse response1, Transaction transaction) {
        if (response1.getPayment() != null) {
            final var isFatal = response1.getPayment().getFatal_error();
            final var isFinal = response1.getPayment().getFinal_status();
            final var status = response1.getPayment().getStatus();
            final var message = response1.getPayment().getMessage();

            if (response1.getResult_code().getValue().equals("0")) {
                switch (checkQiwiStatus(isFatal, isFinal, status)) {
                    case DECLINED:
                        logger.info(BAD_MES
                                , transaction, status, message);

                        declineTransaction(transaction, BANK_DECLINED, message);
                        break;
                    case MANUAL:
                        logger.info("Списание требует ручной обработки - {}, статус - {}"
                                , transaction, status);

                        saveTransaction(transaction, 0, MANUAL, "Ожидает подтверждения Банка", message);
                        break;
                    case OK:
                        logger.info("Заявка на списание средств ждёт выполнения для транзакции - {}, статус - {}"
                                , transaction, response1.getPayment().getStatus());
                        break;
                    case ACCEPTED:
                        handleSuccessOperation(response1, transaction);
                        break;
                }
            } else {
                logger.info(BAD_MES
                        , transaction, status, message);

                declineTransaction(transaction, BANK_DECLINED, message);
            }
        }

        return CompletableFuture.completedFuture(transaction);
    }

    private void handleSuccessOperation(QiwiResponse response1, Transaction transaction) {
        updateQiwiBalance(response1, transaction);

        confirmTransaction(transaction);

        logger.info("Списание средств успешно для транзакции - {}, статус - {}"
                , transaction, response1.getPayment().getStatus());
    }

    public void updateQiwiBalance(QiwiResponse response1, Transaction transaction) {
        final var transactionCurrency = String.valueOf(getCurrency(transaction.getAmount().getCurrency()));
        response1.getBalances().forEach(balance -> {
            if (balance.getCode().equals(transactionCurrency)) {
                manualRepository.findAndModifyGateSet(transaction.getGateId(), online.cryptopie.models.transaction.Balance.builder()
                        .currency(transaction.getAmount().getCurrency())
                        .amount((long) (Double.parseDouble(balance.getValue()) * 100))
                        .build());
            }
        });
    }

    void confirmTransaction(Transaction transaction) {
        if (transaction != null) {
            saveTransaction(transaction, 2, SUCCESS, "", "");
            logger.info("Транзакция успешно принята(confirmTransaction) - {}"
                    , transaction);
        } else {
            logger.error("Для подтверждения транзакции передали не существующую транзакцию");
        }
    }

    @SneakyThrows
    public void declineTransaction(Transaction transaction, String errorCause, String systemError) {
        if (transaction != null) {
            final var result = retryTemplate.execute(context -> {
                var def = new DefaultTransactionDefinition();
                def.setName("forTr" + transaction.getTransactionNumber());
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

                TransactionStatus status = transactionManager.getTransaction(def);
                try {
                    if ((manualRepository.findAndModifyAccountAdd(transaction.getSenderCredentials().getAccount(), transaction.getFinalAmount()) == null)) {
                        logger.error("Во время отмены транзакции аккаунт не был найден! - {}"
                                , transaction);

                        transactionManager.rollback(status);
                        return null;
                    } else {
                        transactionManager.commit(status);

                        return new Object();
                    }
                } catch (Exception ex) {
                    transactionManager.rollback(status);
                    logger.error("Ошибка при возврате средств", ex);
                    throw ex;
                }
            });

            if (result != null) {
                saveTransaction(transaction, 1, DENIED, errorCause, systemError);

                logger.error("Успешный возврат! - {}", transaction);
            }
        } else {
            logger.error("Для отмены транзакции передали не существующую транзакцию");
        }
    }

    public void saveTransaction(Transaction transaction, int stage, int status, String errorReason, String systemError) {
        if (status == DENIED || status == SUCCESS) {
            transaction.setEndDate(new Date());
            if (status == DENIED || status == MANUAL) {
                transaction.setErrorReason(errorReason);
                transaction.setSystemError(systemError);
            }
        }
        transaction.setStatus(status);
        transaction.setStage(stage);

        manualRepository.saveTransaction(transaction);
    }

    private QiwiStatuses checkQiwiStatus(boolean isFatal, boolean isFinal, int status) {
        if (isFatal) {
            return QiwiStatuses.DECLINED;
        }

        if (isFinal) {
            if (status == 60) {
                return QiwiStatuses.ACCEPTED;
            }
            if (status > 99) {
                return QiwiStatuses.DECLINED;
            }
        } else {
            if (status == -1) {
                return QiwiStatuses.DECLINED;
            }
            if (status > -1 && status < 50) {
                return QiwiStatuses.MANUAL;
            }
            if (status > 49 && status < 60) {
                return QiwiStatuses.OK;
            }
        }

        return QiwiStatuses.DECLINED;
    }
}
