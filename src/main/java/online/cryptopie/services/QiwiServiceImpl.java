package online.cryptopie.services;

import lombok.SneakyThrows;
import online.cryptopie.app.QiwiProperties;
import online.cryptopie.models.Gate;
import online.cryptopie.models.QiwiBean;
import online.cryptopie.models.QiwiHTTPRequest;
import online.cryptopie.models.qiwi.PrivateCredentials;
import online.cryptopie.models.qiwi.pay.Request;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.tasks.QiwiTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.ArrayList;

import static online.cryptopie.models.transaction.Status.DENIED;
import static online.cryptopie.models.transaction.Status.WAITING;

@Service
@Async
public class QiwiServiceImpl implements RunnerService {
    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);

    private final RetryTemplate retryTemplate;
    private final PlatformTransactionManager transactionManager;
    private final ManualRepository manualRepository;
    private final QiwiHTTPRequest qiwiHTTPRequest;
    private final QiwiBean qiwiBean;

    public QiwiServiceImpl(RetryTemplate retryTemplate, PlatformTransactionManager transactionManager, ManualRepository manualRepository, QiwiHTTPRequest qiwiHTTPRequest, QiwiBean qiwiBean) {
        this.retryTemplate = retryTemplate;
        this.transactionManager = transactionManager;
        this.manualRepository = manualRepository;
        this.qiwiHTTPRequest = qiwiHTTPRequest;
        this.qiwiBean = qiwiBean;
    }

    public static int getCurrency(String currency) {
        switch (currency) {
            case "RUB":
                return 643;
            case "EUR":
                return 978;
            default:
                return 0;
        }
    }

    @Override
    @SneakyThrows
    public void withdraw(Transaction transaction, Gate gate) {
        logger.info("Обработка транзакции на вывод");
        if (transaction.getStage() != 3) {
            Object result = null;
            try {
                result = retryTemplate.execute(context -> accountSub(transaction));
            } catch (Exception ex) {
                logger.error("Списание с аккаунта при выводе не удалась!", ex);
                qiwiBean.saveTransaction(transaction, 0, WAITING, null, null);
            }

            if (result == null) {
                logger.error("При попытке списания средств с аккаунта произошла ошибка!\n {}", transaction);
                return;
            }

            logger.info("Списание с аккаунта успешно");
        }

        Request request = null;

        var receiverCredentials = transaction.getReceiverCredentials();
        var qiwiCredentials = gate.getQiwiCredentials();

        var terminalId = qiwiCredentials.getTerminal_id();
        var password = qiwiCredentials.getPassword();
        var serviceCards = QiwiProperties.service_id_qiwi_cards;
        var serviceWallet = QiwiProperties.service_id_qiwi_wallets;
        var privateCredentials = PrivateCredentials.builder()
                .certificate(qiwiCredentials.getCertificate())
                .privateKey(qiwiCredentials.getPrivateKey())
                .build();

        if (receiverCredentials.getDestination().equals("card")) {
            request = Request.buildRequest("pay",
                    terminalId,
                    password,
                    "+79999999999",
                    receiverCredentials.getCardNumber(),
                    transaction.getAmount().getCurrency(),
                    gate.getBalance().getCurrency(),
                    String.valueOf(transaction.getAmount().getAmount().doubleValue() / 100),
                    serviceCards,
                    String.valueOf(transaction.getTransactionNumber()),
                    privateCredentials);
        }
        if (receiverCredentials.getDestination().equals("wallet")) {
            if (receiverCredentials.getPhone().charAt(0) == '+')
                receiverCredentials.setPhone(receiverCredentials.getPhone().replace("+", ""));

            request = Request.buildRequest("pay",
                    terminalId,
                    password,
                    receiverCredentials.getPhone(),
                    transaction.getAmount().getCurrency(),
                    gate.getBalance().getCurrency(),
                    String.valueOf(transaction.getAmount().getAmount().doubleValue() / 100),
                    serviceWallet,
                    String.valueOf(transaction.getTransactionNumber()),
                    privateCredentials);

        }
        qiwiHTTPRequest.withdrawQiwi(request)
                .whenComplete((qiwiResponse, throwable) -> {
                    if (qiwiResponse != null && throwable == null) {
                        qiwiBean.handleWithdraw(qiwiResponse, transaction);
                    } else {
                        declineTransaction(transaction, "Ошибка при  проведении платежа", "Ошибка при получении ответа от Qiwi");

                        logger.info("Списание средств неуспешно для транзакции - {}, сообщение - Не удалось получить ответ от QIWI"
                                , transaction);
                    }
                });
    }

    @Override
    public void checkTransaction(Transaction transaction, Gate gate) {
        Request request = null;
        online.cryptopie.models.qiwi.checkStatus.Request check = null;

        var receiverCredentials = transaction.getReceiverCredentials();
        var qiwiCredentials = gate.getQiwiCredentials();

        var terminalId = qiwiCredentials.getTerminal_id();
        var password = qiwiCredentials.getPassword();
        var serviceCards = QiwiProperties.service_id_qiwi_cards;

        var privateCredentials = PrivateCredentials.builder()
                .certificate(qiwiCredentials.getCertificate())
                .privateKey(qiwiCredentials.getPrivateKey())
                .build();

        if (receiverCredentials.getDestination().equals("card")) {
            request = Request.buildRequest("pay",
                    terminalId,
                    password,
                    "+79999999999",
                    receiverCredentials.getCardNumber(),
                    transaction.getAmount().getCurrency(),
                    gate.getBalance().getCurrency(),
                    String.valueOf(transaction.getAmount().getAmount().doubleValue() / 100),
                    serviceCards,
                    String.valueOf(transaction.getTransactionNumber()),
                    privateCredentials);
        }
        if (receiverCredentials.getDestination().equals("wallet")) {
            check = online.cryptopie.models.qiwi.checkStatus.Request.buildRequest(receiverCredentials.getPhone(),
                    String.valueOf(transaction.getTransactionNumber()),
                    password,
                    "pay",
                    terminalId,
                    privateCredentials);
        }

        qiwiHTTPRequest.checkTransactionQiwi(check, request, transaction.getTransactionNumber())
                .whenComplete((qiwiResponse, throwable) -> {
                    if (qiwiResponse != null && throwable == null) {
                        qiwiBean.handleCheckTransaction(qiwiResponse, transaction);
                    }
                });
    }

    @Override
    public void ping() {
        final var requests = new ArrayList<online.cryptopie.models.qiwi.ping.Request>();
        final var gates = manualRepository.findQiwiGates();

        for (Gate gate : gates) {
            final var qiwiCredentials = gate.getQiwiCredentials();
            online.cryptopie.models.qiwi.ping.Request requestPingQiwi = online.cryptopie.models.qiwi.ping.Request.buildRequest(
                    qiwiCredentials.getPassword(),
                    "ping",
                    qiwiCredentials.getTerminal_id());
            requestPingQiwi.setObjectId(gate.getId());
            requestPingQiwi.setCurrency(String.valueOf(getCurrency(gate.getBalance().getCurrency())));
            PrivateCredentials privateCredentials = PrivateCredentials.builder()
                    .certificate(gate.getQiwiCredentials().getCertificate())
                    .privateKey(gate.getQiwiCredentials().getPrivateKey())
                    .build();
            requestPingQiwi.setPrivateCredentials(privateCredentials);

            requests.add(requestPingQiwi);
        }

        for (online.cryptopie.models.qiwi.ping.Request request : requests) {
            qiwiHTTPRequest.ping(request).whenComplete((response, throwable) -> {
                if (response != null && throwable == null) {
                    qiwiBean.handlePing(response);
                }
            });
        }
    }

    @Override
    public void declineTransaction(Transaction transaction, String errorCause, String systemError) {
        qiwiBean.declineTransaction(transaction, errorCause, systemError);
    }

    private Object accountSub(Transaction transaction) {
        var def = new DefaultTransactionDefinition();
        def.setName("forTr" + transaction.getTransactionNumber());
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = transactionManager.getTransaction(def);
        try {
            logger.info("Начало списания средств с аккаунта");
            final var senderAccount = manualRepository.findAndModifyAccountSub(transaction.getSenderCredentials().getAccount()
                    , transaction.getFinalAmount());
            if (senderAccount == null) {
                logger.error("Аккаунт с указанным ID не найден! Для транзакции {}", transaction);

                transactionManager.rollback(status);
                qiwiBean.saveTransaction(transaction, 2, DENIED, "Аккаунт отправителя не найден!", "Аккаунт отправителя не найден!");

                return null;
            }
            if (senderAccount.getBalance().getAmount() < 0) {
                logger.error("Недостаточно средств на аккаунте отправителя! Для транзакции {}", transaction);

                transactionManager.rollback(status);
                qiwiBean.saveTransaction(transaction, 2, DENIED, "INSUFFICIENT_FUNDS", "INSUFFICIENT_FUNDS");

                return null;
            }

        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("Ошибка при списании с аккаунта в выводе", ex);
            throw ex;
        }

        logger.info("Коммитим списание");
        transactionManager.commit(status);
        return new Object();
    }
}
