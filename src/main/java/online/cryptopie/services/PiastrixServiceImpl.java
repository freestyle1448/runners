package online.cryptopie.services;

import online.cryptopie.models.Gate;
import online.cryptopie.models.transaction.Transaction;
import org.springframework.stereotype.Service;

@Service
public class PiastrixServiceImpl implements RunnerService {
    @Override
    public void declineTransaction(Transaction transaction, String errorCause, String systemError) {

    }

    @Override
    public void withdraw(Transaction transaction, Gate gate) {

    }

    @Override
    public void ping() {
        return;
    }

    @Override
    public void checkTransaction(Transaction transaction, Gate gate) {

    }


    /*private final TransactionsRepository transactionsRepository;
    private final RestTemplate restTemplate;
    private final ManualRepository manualRepository;
    private final GatesRepository gatesRepository;
    private final CurrenciesRepository currenciesRepository;
    private final Gson gson = new Gson();

    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss.ssssss");


    @Autowired
    public PiastrixServiceImpl(TransactionsRepository transactionsRepository, RestTemplate restTemplate, ManualRepository manualRepository, GatesRepository gatesRepository, CurrenciesRepository currenciesRepository) {
        this.transactionsRepository = transactionsRepository;
        this.restTemplate = restTemplate;
        this.manualRepository = manualRepository;
        this.gatesRepository = gatesRepository;
        this.currenciesRepository = currenciesRepository;
    }

    @Override
    public void saveTransaction(Transaction transaction) {
        transactionsRepository.save(transaction);
    }

    @Transactional
    private void confirmTransaction(Transaction transaction) {
        if (transaction != null) {
            if (transaction.getHistoryList() == null) {
                transaction.setHistoryList(new ArrayList<>());
                transaction.getHistoryList().add(History.builder().
                        date(new Date())
                        .prevStatus(transaction.getStatus())
                        .curStatus(SUCCESS)
                        .build());
            }
            transaction.setStatus(SUCCESS);
            manualRepository.findAndUpdateGateCommissionAndAccount(transaction.getGateId(), transaction.getCommission());
            transactionsRepository.save(transaction);

        } else {
            throw new NotFoundException("Запрашиваемая Вами транзакция не найдена!");
        }
    }

    @Override
    @Transactional
    public CompletableFuture<Transaction> declineTransaction(Transaction transaction, String errorCause) throws MongoException {
        if (transaction == null) {
            throw new NotFoundException("Запрашиваемая Вами транзакция не найдена!");
        }

        if (manualRepository.findAndModifyAccountAdd(transaction.getAccountId(), transaction.getFinalAmount()) != null) {
            if (manualRepository.findAndModifyGateAdd(transaction.getGateId(), transaction.getFinalAmount()) != null) {
                if (transaction.getHistoryList() == null) {
                    transaction.setHistoryList(new ArrayList<>());
                    transaction.getHistoryList().add(History.builder()
                            .date(new Date())
                            .prevStatus(transaction.getStatus())
                            .curStatus(DENIED)
                            .build());
                } else {
                    transaction.getHistoryList().add(History.builder()
                            .date(new Date())
                            .prevStatus(transaction.getStatus())
                            .curStatus(DENIED)
                            .build());
                }

                transaction.setErr(errorCause);
                transaction.setStatus(DENIED);
                return CompletableFuture.completedFuture(manualRepository.declineTransaction(transaction));
            } else {
                throw new NotFoundException("Гейт с указанным ID не найден!");
            }
        } else {
            throw new NotFoundException("Аккаунт с указанным ID не найден!");
        }

    }

    @Override
    public CompletableFuture<Transaction> withdraw(Transaction transaction) throws ResourceAccessException {
        Receiver receiver = null;
        for (Receiver rec : transaction.getReceiverCredentials())
            if (rec.getName().equals(PIASTRIX_CREDENTIALS))
                receiver = rec;

        assert receiver != null;
        Request withdrawTry = Request.builder()
                .shop_id(SHOP_ID)
                .amount(transaction.getFinalAmount().getAmount().doubleValue())
                .shop_currency(Balance.getMonetary(transaction.getFinalAmount()).getCurrency().getNumericCode())
                .amount_type(transaction.getAmountType())
                .payway(receiver.getValue())
                .build();
        withdrawTry.genSign();
        final String responseWithdrawTryString = restTemplate.postForObject(DEBUGGING_URL
                , withdrawTry, String.class);

        final Response responseWithdrawTry = gson.fromJson(responseWithdrawTryString, Response.class);

        if (responseWithdrawTry.getResult()) {
            restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                    .src(SOURCE_LOG)
                    .log(String.format("Предварительный расчет выплаты успешно для транзакции %d", transaction.getTransactionNumber()))
                    .build(), String.class);
            piastrixLogger.printMes(String.format("Предварительный расчет выплаты успешно для транзакции %d", transaction.getTransactionNumber()));

            final online.cryptopie.models.piastrix.check_account.Request checkAccount = online.cryptopie.models.piastrix.check_account.
                    Request.builder()
                    .account("41001") //TODO вернут responseWithdrawTry.getData().getAccount_info_config().getAccount().getTitle()
                    .payway(withdrawTry.getPayway())
                    .amount(withdrawTry.getAmount())
                    .shop_id(withdrawTry.getShop_id())
                    .build();
            checkAccount.setSign();

            final String responseCheckAccountString = restTemplate.postForObject(DEBUGGING_URL_CHECK_ACC
                    , checkAccount, String.class);

            final online.cryptopie.models.piastrix.check_account.Response responseAccountCheck =
                    gson.fromJson(responseCheckAccountString, online.cryptopie.models.piastrix.check_account.Response.class);

            if (responseAccountCheck.getResult() && responseAccountCheck.getData().getProvider_status() == 1) {
                restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                        .src(SOURCE_LOG)
                        .log(String.format("Проверка аккаунта на возможность пополнения успешно для транзакции %d", transaction.getTransactionNumber()))
                        .build(), String.class);
                piastrixLogger.printMes(String.format("Проверка аккаунта на возможность пополнения успешно для транзакции %d", transaction.getTransactionNumber()));

                final online.cryptopie.models.piastrix.withdraw.Request withdraw =
                        online.cryptopie.models.piastrix.withdraw.Request.builder()
                                .account("41001") //TODO смотри выше
                                .amount_type(withdrawTry.getAmount_type())
                                .amount(withdrawTry.getAmount())
                                .payway(withdrawTry.getPayway())
                                .shop_currency(withdrawTry.getShop_currency())
                                .shop_id(withdrawTry.getShop_id())
                                .shop_payment_id(String.valueOf(System.currentTimeMillis()))
                                .build();
                withdraw.genSign();

                final String responseWithdrawString = restTemplate.postForObject(DEBUGGING_URL_WITHDRAW
                        , withdraw, String.class);

                final online.cryptopie.models.piastrix.withdraw.Response responseWithdraw =
                        gson.fromJson(responseWithdrawString, online.cryptopie.models.piastrix.withdraw.Response.class);

                if (responseWithdraw.getResult()) {
                    restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                            .src(SOURCE_LOG)
                            .log(String.format("Вывод средств в других валютах с баланса магазина успешно для транзакции %d, заявка создана", transaction.getTransactionNumber()))
                            .build(), String.class);
                    piastrixLogger.printMes(String.format("Вывод средств в других валютах с баланса магазина успешно для транзакции %d, заявка создана", transaction.getTransactionNumber()));

                    transaction.setData(responseWithdraw.getData());
                    final Optional<Gate> gateOptional = gatesRepository.findById(transaction.getGateId());
                    Gate gate = null;
                    if (gateOptional.isPresent()) {
                        gate = gateOptional.get();
                    }
                    assert gate != null;
                    gate.getBalance().setAmount(responseWithdraw.getData().getBalance());
                    gatesRepository.save(gate);
                    transactionsRepository.save(transaction);
                } else {
                    restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                            .src(SOURCE_LOG)
                            .log(String.format("Вывод средств в других валютах с баланса магазина неуспешно для транзакции %d, с ошибкой %s транзакция отменена"
                                    , transaction.getTransactionNumber(), responseWithdraw.getMessage()))
                            .build(), String.class);
                    piastrixLogger.printMes(String.format("Вывод средств в других валютах с баланса магазина неуспешно для транзакции %d, с ошибкой %s транзакция отменена"
                            , transaction.getTransactionNumber(), responseWithdraw.getMessage()));

                    declineCheck(responseWithdraw.getError_code(), transaction, responseWithdraw.getMessage());
                    locker.unlock(PIASTRIX_PAY_LOCK);
                    throw new RuntimeException(responseWithdraw.getMessage());
                }
            } else {
                restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                        .src(SOURCE_LOG)
                        .log(String.format("Проверка аккаунта на возможность пополнения неуспешно для транзакции %d, с ошибкой %s транзакция отменена"
                                , transaction.getTransactionNumber(), responseAccountCheck.getMessage()))
                        .build(), String.class);
                piastrixLogger.printMes(String.format("Проверка аккаунта на возможность пополнения неуспешно для транзакции %d, с ошибкой %s транзакция отменена"
                        , transaction.getTransactionNumber(), responseAccountCheck.getMessage()));

                declineCheck(responseAccountCheck.getError_code(), transaction, responseAccountCheck.getMessage());
                locker.unlock(PIASTRIX_PAY_LOCK);
                throw new RuntimeException(responseAccountCheck.getMessage());
            }
        } else {
            restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                    .src(SOURCE_LOG)
                    .log(String.format("Предварительный расчет выплаты неуспешно для транзакции %d, с ошибкой %s транзакция отменена"
                            , transaction.getTransactionNumber(), responseWithdrawTry.getMessage()))
                    .build(), String.class);
            piastrixLogger.printMes(String.format("Предварительный расчет выплаты неуспешно для транзакции %d, с ошибкой %s транзакция отменена"
                    , transaction.getTransactionNumber(), responseWithdrawTry.getMessage()));

            declineCheck(responseWithdrawTry.getError_code(), transaction, responseWithdrawTry.getMessage());
            locker.unlock(PIASTRIX_PAY_LOCK);
            throw new RuntimeException(responseWithdrawTry.getMessage());
        }

        return CompletableFuture.completedFuture(transaction);
    }

    @Override
    @Transactional
    public CompletableFuture<Transaction> checkTransaction(Transaction transaction) throws ResourceAccessException {
        online.cryptopie.models.piastrix.withdraw_id.Request withdraw_id = online.cryptopie.models.piastrix.withdraw_id.Request.builder()
                .now(new Date().toString())
                .shop_id(SHOP_ID)
                .withdraw_id(transaction.getData().getId())
                .build();

        withdraw_id.genSign();

        final String responseWithdrawIdString = restTemplate.postForObject(DEBUGGING_URL_WITHDRAW_ID
                , withdraw_id, String.class);

        final online.cryptopie.models.piastrix.withdraw_id.Response responseWithdrawId =
                gson.fromJson(responseWithdrawIdString, online.cryptopie.models.piastrix.withdraw_id.Response.class);

        if (responseWithdrawId.getResult()) {
            int error_code = responseWithdrawId.getData().getStatus();

            if (error_code == 5) {
                restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                        .src(SOURCE_LOG)
                        .log(String.format("Запрос статуса выплаты по id успешно для транзакции %d, со статусом %d"
                                , transaction.getTransactionNumber(), error_code))
                        .build(), String.class);
                piastrixLogger.printMes(String.format("Запрос статуса выплаты по id успешно для транзакции %d, со статусом %d"
                        , transaction.getTransactionNumber(), error_code));

                confirmTransaction(transaction);
            }
            if (error_code == 10 || error_code == 11 || error_code == 6) {
                restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                        .src(SOURCE_LOG)
                        .log(String.format("Запрос статуса выплаты по id неуспешно для транзакции %d, со статусом %d и ошибкой %s"
                                , transaction.getTransactionNumber(), error_code, responseWithdrawId.getMessage()))
                        .build(), String.class);
                piastrixLogger.printMes(String.format("Запрос статуса выплаты по id неуспешно для транзакции %d, со статусом %d и ошибкой %s"
                        , transaction.getTransactionNumber(), error_code, responseWithdrawId.getMessage()));

                declineTransaction(transaction, responseWithdrawId.getMessage());
            }

        } else {
            restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                    .src(SOURCE_LOG)
                    .log(String.format("Предварительный расчет выплаты неуспешно для транзакции %d, с ошибкой %s транзакция отменена"
                            , transaction.getTransactionNumber(), responseWithdrawId.getMessage()))
                    .build(), String.class);
            piastrixLogger.printMes(String.format("Предварительный расчет выплаты неуспешно для транзакции %d, с ошибкой %s транзакция отменена"
                    , transaction.getTransactionNumber(), responseWithdrawId.getMessage()));

            declineCheck(responseWithdrawId.getErrorCode(), transaction, responseWithdrawId.getMessage());
            locker.unlock(PIASTRIX_CONFIRM_LOCK);
            throw new RuntimeException(responseWithdrawId.getMessage());
        }

        return CompletableFuture.completedFuture(transaction);
    }

    @Override
    public void ping() throws ResourceAccessException {
        final online.cryptopie.models.piastrix.shop_balance.Request shop_balance = online.cryptopie.models.piastrix.shop_balance.Request.builder()
                .now(dateFormatter.format(new Date()))
                .shop_id(SHOP_ID)
                .build();
        shop_balance.genSign();

        final String responseShopBalanceString = restTemplate.postForObject(DEBUGGING_URL_SHOP_BALANCE
                , shop_balance, String.class);
        final online.cryptopie.models.piastrix.shop_balance.Response responseShopBalance =
                gson.fromJson(responseShopBalanceString, online.cryptopie.models.piastrix.shop_balance.Response.class);

        if (responseShopBalance.getResult()) {
            *//*restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                    .src(SOURCE_LOG)
                    .log("Запрос баланса по магазину")
                    .build(), String.class);*//*

            final Gate gate = gatesRepository.findByShopId(SHOP_ID);
            Currency currency = currenciesRepository.findByCurrency(gate.getBalance().getCurrency());

            List<online.cryptopie.models.piastrix.shop_balance.Balance> balanceList = new ArrayList<>();
            responseShopBalance.getData().getBalances()
                    .parallelStream()
                    .filter(balance -> balance.getCurrency().equals(currency.getCode()))
                    .forEach(balanceList::add);
            if (balanceList.size() == 1) {
                gate.getBalance().setAmount(balanceList.get(0).getAvailable().doubleValue() * 100);
                gatesRepository.save(gate);
            }

        } else {
            restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                    .src(SOURCE_LOG)
                    .log(String.format("Запрос баланса по магазину неуспешно , с ошибкой %s"
                            , responseShopBalance.getMessage()))
                    .build(), String.class);
            locker.unlock(Locker.PIASTRIX_PING_LOCK);
            throw new RuntimeException(responseShopBalance.getMessage());
        }
        locker.unlock(Locker.PIASTRIX_PING_LOCK);
    }

    @Transactional
    private void declineCheck(int error_code, Transaction transaction, String message) {
        if (error_code == 10 || error_code == 11 || error_code == 6 || error_code == 4 || error_code == 3)
            declineTransaction(transaction, message);
    }*/
}
