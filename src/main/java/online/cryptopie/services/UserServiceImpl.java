package online.cryptopie.services;

import lombok.SneakyThrows;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.tasks.QiwiTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.Date;

import static online.cryptopie.models.transaction.Status.DENIED;
import static online.cryptopie.models.transaction.Status.SUCCESS;

@Service
@Async
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);

    private final RetryTemplate retryTemplate;
    private final PlatformTransactionManager transactionManager;
    private final ManualRepository manualRepository;

    public UserServiceImpl(RetryTemplate retryTemplate, PlatformTransactionManager transactionManager, ManualRepository manualRepository) {
        this.retryTemplate = retryTemplate;
        this.transactionManager = transactionManager;
        this.manualRepository = manualRepository;
    }

    @Override
    @SneakyThrows
    public void createExchangeTransaction(Transaction transaction) {
        final var result = retryTemplate.execute(context -> {
            var def = new DefaultTransactionDefinition();
            def.setName("forTr" + transaction.getTransactionNumber());
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

            TransactionStatus status = transactionManager.getTransaction(def);
            try {
                final var senderAccount = manualRepository.findAndModifyAccountSub(transaction.getSenderCredentials().getAccount()
                        , transaction.getAmount());
                if (senderAccount == null) {
                    logger.error("Аккаунт с указанным ID не найден! Для транзакции {}", transaction);

                    transactionManager.rollback(status);
                    saveTransaction(transaction, 2, DENIED, "Аккаунт отправителя не найден!");

                    return null;
                }
                if (senderAccount.getBalance().getAmount() < 0) {
                    logger.error("Недостаточно средств на аккаунте отправителя! Для транзакции {}", transaction);

                    transactionManager.rollback(status);
                    saveTransaction(transaction, 2, DENIED, "INSUFFICIENT_FUNDS");

                    return null;
                }
                logger.info("Списание с аккаунта отправителя успешно");

                if (manualRepository.findAndModifyAccountAdd(transaction.getReceiverCredentials().getAccount()
                        , transaction.getAmount()) == null) {
                    logger.error("Аккаунт получателя с указанным ID не найден! Для транзакции {}", transaction);

                    transactionManager.rollback(status);
                    saveTransaction(transaction, 2, DENIED, "Аккаунт получателя не найден!");

                    return null;
                }
            } catch (Exception ex) {
                transactionManager.rollback(status);
                logger.error("Ошибка при переводе средств между аккаунтами", ex);
                throw ex;
            }

            transactionManager.commit(status);

            return new Object();
        });

        if (result != null) {
            logger.info("Пополнение аккаунта получателя успешно");
            saveTransaction(transaction, 2, SUCCESS, "");
        }
    }

    @Override
    public void inTransaction(Transaction transaction) {
        var errorReason = "";
        int stage;
        int status;

        if (manualRepository.findAndModifyAccountAdd(transaction.getReceiverCredentials().getAccount(),
                transaction.getFinalAmount()) != null) {
            status = SUCCESS;
            stage = 2;

            logger.error("Успешный ввод {}", transaction);
        } else {
            stage = 2;
            status = DENIED;
            errorReason = "Аккаунт не найден";
            logger.error("Аккаунт не найден на входе {}", transaction);
        }

        saveTransaction(transaction, stage, status, errorReason);
    }

    private void saveTransaction(Transaction transaction, int stage, int status, String errorReason) {
        if (status == DENIED || status == SUCCESS) {
            transaction.setEndDate(new Date());
            if (status == DENIED) {
                transaction.setErrorReason(errorReason);
            }
        }
        transaction.setStatus(status);
        transaction.setStage(stage);

        manualRepository.saveTransaction(transaction);
    }
}
