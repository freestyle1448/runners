package online.cryptopie.services;

import online.cryptopie.models.Gate;
import online.cryptopie.models.transaction.Transaction;

public interface RunnerService {
    void declineTransaction(Transaction transaction, String errorCause, String systemError);

    void withdraw(Transaction transaction, Gate gate);

    void ping();

    void checkTransaction(Transaction transaction, Gate gate);
}
