package online.cryptopie.services;

import online.cryptopie.models.transaction.Transaction;

public interface UserService {
    void createExchangeTransaction(Transaction transaction);

    void inTransaction(Transaction transaction);
}
