package online.cryptopie.repositories;

import online.cryptopie.models.Gate;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GatesRepository extends MongoRepository<Gate, ObjectId> {
}

