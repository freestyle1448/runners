package online.cryptopie.repositories;

import online.cryptopie.models.QiwiRunnerError;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QiwiErrorsRepository extends MongoRepository<QiwiRunnerError, ObjectId> {
}
