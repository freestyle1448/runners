package online.cryptopie.exception;

public class CryptopieException extends RuntimeException {
    public CryptopieException(String message) {
        super(message);
    }
}