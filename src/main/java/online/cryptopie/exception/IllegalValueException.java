package online.cryptopie.exception;

public class IllegalValueException extends CryptopieException {
    public IllegalValueException(String message) {
        super(message);
    }
}
