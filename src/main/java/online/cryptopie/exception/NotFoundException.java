package online.cryptopie.exception;

public class NotFoundException extends CryptopieException {
    public NotFoundException(String message) {
        super(message);
    }
}