package online.cryptopie.exception;

public class NotEnoughMoneyException extends CryptopieException {
    public NotEnoughMoneyException(String message) {
        super(message);
    }
}
