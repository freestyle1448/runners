package online.cryptopie.tasks;

import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.FullDocument;
import online.cryptopie.models.QiwiRunnerError;
import online.cryptopie.models.RequestTypes;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.GatesRepository;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.repositories.QiwiErrorsRepository;
import online.cryptopie.services.RunnerService;
import online.cryptopie.services.UserService;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.messaging.ChangeStreamRequest;
import org.springframework.data.mongodb.core.messaging.MessageListener;
import org.springframework.data.mongodb.core.messaging.MessageListenerContainer;
import org.springframework.data.mongodb.core.messaging.Subscription;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static online.cryptopie.models.RequestTypes.EXCHANGE;
import static online.cryptopie.models.RequestTypes.IN;
import static online.cryptopie.models.transaction.Status.WAITING;
import static online.cryptopie.models.transaction.Type.*;

@Component
public class TransactionsChangeStream {
    public static final String STATUS = "status";
    public static final String STAGE = "stage";
    public static final String TRANSACTIONS = "transactions";

    private static final Logger logger = LoggerFactory.getLogger(QiwiTasks.class);
    private final QiwiErrorsRepository qiwiErrorsRepository;

    private final GatesRepository gatesRepository;
    private final ManualRepository manualRepository;
    private final MessageListenerContainer messageListenerContainer;
    private final RunnerService qiwiService;
    private final UserService userService;
    private final QiwiTasks qiwiTasks;

    public TransactionsChangeStream(GatesRepository gatesRepository, ManualRepository manualRepository, MessageListenerContainer messageListenerContainer,
                                    @Qualifier("qiwiServiceImpl") RunnerService qiwiService, UserService userService, QiwiTasks qiwiTasks, QiwiErrorsRepository qiwiErrorsRepository) {
        this.gatesRepository = gatesRepository;
        this.manualRepository = manualRepository;
        this.messageListenerContainer = messageListenerContainer;
        this.qiwiService = qiwiService;
        this.userService = userService;
        this.qiwiTasks = qiwiTasks;
        this.qiwiErrorsRepository = qiwiErrorsRepository;
    }


    @PostConstruct
    public void postConstruct() {
        try {
            qiwiTasks.pay();
            qiwiTasks.exchange();
        } catch (Exception e) {
            logger.error("Ошибка при вызове метода восстановления после сбоя!", e);
        }
        System.out.println(subscribeToQiwiOut().isActive());
        System.out.println(subscribeToExchange().isActive());
        //System.out.println(subscribeToIn().isActive());
    }

    public Subscription subscribeToQiwiOut() {
        final var qiwiOutTransaction = Aggregation.newAggregation(Aggregation.match(Criteria
                .where(STATUS).is(WAITING)
                .and(STAGE).is(0)
                .and("type").is(USER_OUT)));

        MessageListener<ChangeStreamDocument<Document>, Transaction> messageListener = message -> {
            try {
                Transaction newTransaction = message.getBody();
                if (newTransaction != null) {
                    final var gateOptional = gatesRepository.findById(newTransaction.getGateId());

                    if (gateOptional.isPresent()) {
                        final var gate = gateOptional.get();

                        if (gate.getGroup().equals("qiwi")) {
                            final var qiwiTransactionUpdated = manualRepository.findAndModifyQiwiTransaction(newTransaction.getId());
                            logger.info("Вызов метода out для транзакции - {}", newTransaction);

                            qiwiService.withdraw(qiwiTransactionUpdated, gate);
                        }
                    }
                }
            } catch (Exception ex) {
                qiwiErrorsRepository.save(QiwiRunnerError.builder()
                        .error(ex.toString())
                        .fullException(ex.fillInStackTrace().toString())
                        .requestType(RequestTypes.WITHDRAW)
                        .build());
            }
        };

        ChangeStreamRequest<Transaction> request = ChangeStreamRequest.builder(messageListener)
                .collection(TRANSACTIONS)
                .filter(qiwiOutTransaction)
                .build();

        messageListenerContainer.start();
        return messageListenerContainer.register(request, Transaction.class);
    }

    public Subscription subscribeToExchange() {
        final var excTransaction = Aggregation.newAggregation(Aggregation.match(Criteria
                .where(STATUS).is(WAITING)
                .and(STAGE).is(0)
                .and("type").is(USER_EXCHANGE)));

        MessageListener<ChangeStreamDocument<Document>, Transaction> messageListener = message -> {
            try {
                Transaction newTransaction = message.getBody();
                if (newTransaction != null) {
                    logger.info("Вызов метода exchange для транзакции - {}", newTransaction);

                    userService.createExchangeTransaction(newTransaction);
                }
            } catch (Exception ex) {
                qiwiErrorsRepository.save(QiwiRunnerError.builder()
                        .error(ex.toString())
                        .fullException(ex.fillInStackTrace().toString())
                        .requestType(EXCHANGE)
                        .build());
            }
        };

        ChangeStreamRequest<Transaction> request = ChangeStreamRequest.builder(messageListener)
                .collection(TRANSACTIONS)
                .fullDocumentLookup(FullDocument.UPDATE_LOOKUP)
                .filter(excTransaction)
                .build();

        messageListenerContainer.start();
        return messageListenerContainer.register(request, Transaction.class);
    }

    public Subscription subscribeToIn() {
        final var inTransactionFilter = Aggregation.newAggregation(Aggregation.match(Criteria
                .where(STATUS).is(WAITING)
                .and(STAGE).is(0)
                .and("amountMethod").is("direct")
                .and("type").is(INCOME)));

        MessageListener<ChangeStreamDocument<Document>, Transaction> messageListener = message -> {
            try {
                Transaction newTransaction = message.getBody();
                if (newTransaction != null) {
                    logger.info("Вызов метода in для транзакции - {}", newTransaction);

                    userService.inTransaction(newTransaction);
                }
            } catch (Exception ex) {
                qiwiErrorsRepository.save(QiwiRunnerError.builder()
                        .error(ex.toString())
                        .fullException(ex.fillInStackTrace().toString())
                        .requestType(IN)
                        .build());
            }
        };

        ChangeStreamRequest<Transaction> request = ChangeStreamRequest.builder(messageListener)
                .collection(TRANSACTIONS)
                .filter(inTransactionFilter)
                .build();

        messageListenerContainer.start();
        return messageListenerContainer.register(request, Transaction.class);
    }
}
