package online.cryptopie.tasks;

import org.springframework.stereotype.Component;

@Component
public class PiastrixTasks {
    /*private final Locker locker = getInstance();
    private final ManualRepository manualRepository;
    private final RunnerService piastrixService;
    private final RestTemplate restTemplate;
    private final TransactionsRepository transactionsRepository;

    public PiastrixTasks(ManualRepository manualRepository, @Qualifier("piastrixServiceImpl") RunnerService piastrixService, RestTemplate restTemplate, TransactionsRepository transactionsRepository) {
        this.manualRepository = manualRepository;
        this.piastrixService = piastrixService;
        this.restTemplate = restTemplate;
        this.transactionsRepository = transactionsRepository;
    }

    //@Scheduled(fixedRate = 10000)
    public void withdrawScheduled() throws ExecutionException, InterruptedException {
        if (locker.isNotLocked(PIASTRIX_PING_LOCK) && locker.isNotLocked(PIASTRIX_PAY_LOCK) && locker.isNotLocked(PIASTRIX_CONFIRM_LOCK)) {
            locker.lock(PIASTRIX_PAY_LOCK);
            List<Transaction> transactions = new ArrayList<>();
            Transaction tr = manualRepository.findAndModifyPiastrixTransaction();
            while (tr != null) {
                transactions.add(tr);
                tr = manualRepository.findAndModifyPiastrixTransaction();
            }

            CompletableFuture<?>[] tasks = transactions.parallelStream()
                    .map(transaction -> {
                        if (transaction.getHistoryList() == null) {
                            transaction.setHistoryList(new ArrayList<>());
                            transaction.getHistoryList()
                                    .add(History.builder()
                                            .date(new Date())
                                            .prevStatus(WAITING)
                                            .curStatus(IN_PROCESS)
                                            .build());
                        }
                        transaction.setAmountType(OUT);
                        transaction.setStatus(IN_PROCESS);
                        transaction.setStage(1);
                        try {
                            if (true) {
                                piastrixService.saveTransaction(transaction);
                                return piastrixService.withdraw(transaction).exceptionally(throwable -> null);
                            } else {
                                return piastrixService.declineTransaction(transaction, "Неверный hash!")
                                        .exceptionally(throwable -> {
                                            throwable.printStackTrace();
                                            return null;
                                        });
                            }
                        } catch (Exception exc) {
                            restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                                    .src(SOURCE_LOG)
                                    .log("При выводе средств возникла ошибка - Неверный hash!")
                                    .build(), String.class);
                            transaction.setStage(0);
                            transaction.setStatus(WAITING);

                            transactionsRepository.save(transaction);
                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .thenRun(() -> locker.unlock(PIASTRIX_PAY_LOCK))
                    .get();
        }
    }

    //@Scheduled(fixedRate = 10000, initialDelay = 500)
    public void checkTransactions() throws ExecutionException, InterruptedException {
        if (locker.isNotLocked(PIASTRIX_PING_LOCK) && locker.isNotLocked(PIASTRIX_PAY_LOCK) && locker.isNotLocked(PIASTRIX_CONFIRM_LOCK)) {
            locker.lock(PIASTRIX_CONFIRM_LOCK);

            List<Transaction> transactions = manualRepository.findQiwiTransaction();

            CompletableFuture<?>[] tasks = transactions.parallelStream()
                    .map(transaction -> {
                        try {
                            if (true) {
                                piastrixService.saveTransaction(transaction);
                                return piastrixService.checkTransaction(transaction).exceptionally(throwable -> null);
                            } else {

                                return piastrixService.declineTransaction(transaction, "Неверный hash!")
                                        .exceptionally(throwable -> {
                                            throwable.printStackTrace();
                                            return null;
                                        });
                            }
                        } catch (Exception exc) {
                            restTemplate.postForObject(LOG_BOT_URL, Log.builder()
                                    .src(SOURCE_LOG)
                                    .log("При выводе средств возникла ошибка - Неверный hash!")
                                    .build(), String.class);
                            return CompletableFuture.completedFuture(null);
                        }
                    })
                    .toArray(CompletableFuture[]::new);

            CompletableFuture.allOf(tasks)
                    .thenRun(() -> locker.unlock(PIASTRIX_CONFIRM_LOCK))
                    .get();
        }
    }


    //@Scheduled(fixedRate = 60000, initialDelay = 1000)
    public void ping() {
        if (locker.isNotLocked(PIASTRIX_PING_LOCK) && locker.isNotLocked(PIASTRIX_PAY_LOCK) && locker.isNotLocked(PIASTRIX_CONFIRM_LOCK)) {
            locker.lock(PIASTRIX_PING_LOCK);
            try {
                piastrixService.ping();
            } catch (Exception ex) {
                locker.unlock(PIASTRIX_PING_LOCK);
            }
        }
    }*/
}